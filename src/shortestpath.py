import numpy as np
from geopy.distance import geodesic
from scipy.sparse import lil_matrix 
from scipy.sparse.csgraph import dijkstra
from json import loads

# Give the json of an overpass API request, builds the graph corresponding to the result
# points of the graph have a local index and an OSM index and
# the translation from one type of index to the other is done in indxs and indxs rev
# 
# coords is a dict, with OSM index as key and [lat, lng] as value.
# 
# resMatrix is the adjacence matrix of the graph, with local index.
def parsejson_tograph(json):
    indxs = []
    indxsrev = {}
    coords = {}
    resE = []
    j = 0
    for e in json["elements"]:
        if e["type"] == "node":
            coords[e["id"]] = np.array([e["lat"], e["lon"]])
            indxs.append(e["id"])
            indxsrev[e["id"]] = j
            j += 1
        if e["type"] == "way":
            for i in range(len(e["nodes"])-1): 
                a = e["nodes"][i]
                b = e["nodes"][i+1]
                resE.append([a,b])

    resMatrix = lil_matrix((j,j))
    for e in resE:
        a,b = indxsrev[e[0]],indxsrev[e[1]]
        resMatrix[a,b] = geodesic(coords[e[0]],coords[e[1]]).m

    print(len(indxs))
    return coords, indxs, indxsrev, resMatrix

# Finds the closest point in the local index list `l` to the point `a` (`a` is in [lat,lng] format).
def closest(a,l,coords):
    res = l[0]
    best = geodesic(a,coords[res]).km
    for e in l:
        d = geodesic(a,coords[e]).km
        if d < best:
            res = e
            best = d
    return res


def flatten(l):
    return [elem for elem in cont for cont in l]

# As Dijkstra returns the predecessor list, (at local index i, will have 
# the local index j of the point just before in the shortest path as value)
def path_from_pred(preds, a, b):
    res = [b]
    actual = b
    while (preds[actual] != a and preds[actual] >= 0):
        actual = preds[actual]
        res.append(actual)
    return res

def shortest(json,a,b):
    print("Calculating path")
    coords, indxs, indxsrev, adjMatrix = parsejson_tograph(json)


    vA = closest(a,indxs,coords)
    vB = closest(b,indxs,coords)

    print(a, coords[vA])

    iA = indxsrev[vA]
    iB = indxsrev[vB]

    dists, ipreds = dijkstra(adjMatrix, directed=False, indices=iA, return_predecessors=True)

    ishortestpath = path_from_pred(ipreds, iA, iB)
    vpath = list(reversed([coords[indxs[e]] for e in ishortestpath]))
    return np.array([coords[vA]]+vpath+[coords[vB]]), dists[iB]
