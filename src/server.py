from flask import Flask, request
from flask_cors import CORS, cross_origin
import geojson
import json
import numpy as np
from makerequest import followRiver
from shortestpath import shortest
from exportgpx import exportgpx

app = Flask(__name__)
CORS(app)

@app.route('/', methods=['GET'])
@cross_origin()
def get_shortest_path():
    # Get parameters from the query string
    to_follow = request.args.get('to_follow')
    a_lat = request.args.get('a_lat', type=float)
    a_lon = request.args.get('a_lon', type=float)
    b_lat = request.args.get('b_lat', type=float)
    b_lon = request.args.get('b_lon', type=float)

    if None in [to_follow, a_lat, a_lon, b_lat, b_lon]: 
        return {"error": "Missing required parameters"}, 400

    # Convert coordinates to numpy arrays
    a = np.array([a_lat, a_lon])
    b = np.array([b_lat, b_lon])
    
    jsondata = ""
    if to_follow[:6] == "river:":
        jsondata = followRiver(to_follow[6:])
        print("River downloaded")


    # Calculate the shortest path
    res, dst = shortest(jsondata, a, b)
    print("Path calculated")

    # Export the result to GPX
    # exportgpx(res, "result.gpx")
    #print("Track wrote")

    trueres = geojson.LineString([list(reversed(list(point))) for point in res])
    truetrueres = geojson.FeatureCollection([geojson.Feature(geometry=trueres,properties={"description" : dst})])

    return truetrueres, 200

if __name__ == '__main__':
    app.run(debug=True)
