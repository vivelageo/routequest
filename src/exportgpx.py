import gpxpy
def exportgpx(track,filename):    
  print("Writing track")
  gpx = gpxpy.gpx.GPX()
  
  gpx_track = gpxpy.gpx.GPXTrack()
  gpx.tracks.append(gpx_track)
  
  gpx_segment = gpxpy.gpx.GPXTrackSegment()
  gpx_track.segments.append(gpx_segment)
  
  for pt in track:
    gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(pt[0],pt[1]))

  f = open(filename, "w")
  f.write(gpx.to_xml())
