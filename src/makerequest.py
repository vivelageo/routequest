import requests
import json


def followRiver(name):
    res = ""
    for key in ["name", "name:en", "name:fr"]:
        print(key)
        thedata = followRiverWithKey(name, key)
        if thedata:
            return thedata
        

def followRiverWithKey(name, key):
    print("Finding river")
    dt = """
[out:json];
(rel["waterway"="river"][\""""+key+""""~\""""+name+"""\"];)->.rh;
(way(r.rh : "main_stream");>>;);
out skel;
    """
    try:
        req = requests.post("https://overpass-api.de/api/interpreter",data=dt)
        if (req.status_code == 200):
            thereponse = json.loads(req.text)
            if (len(thereponse["elements"]) > 0):
                return thereponse
    except:
        pass
